**                NAND2TETRIS PROJECT 1

                GRUPO 1
                
                CARLOS ANDRES GOMEZ ORDUZ - 2202148**
                SANTIAGO MENESES CÁCERES - 2200255**
                SANTIAGO CAMARGO ARDILA - 2211873**



**1. NOT**:

Tenemos una entrada y una salida, como partimos de la compuerta NAND donde tenemos la salida de 1 en todos los casos menos en el que ambas entradas son 1 y queremos llegar a la NOT donde si tenemos 1 en una entrada la salida será 0 y si tenemos 0 en una entrada la salida será 1, entonces simplemente debemos ingresar la misma entrada en ambas entradas a y b de la compuerta NAND

**2.	AND**

Para la compuerta AND sabemos que la salida siempre es 0 a menos que las entradas sean 1 y en la compuerta NAND donde tenemos la salida de 1 en todos los casos menos en el que ambas entradas son 1 entonces para obtener la compuerta and negamos la salida 'NandOut' de la compuerta NAND.

**3.	OR**

En esta compuerta la salida siempre es 1 a menos de que ambas entradas son 0, entonces para obtenerla simplemente negamos las entradas a y b de la compuerta NAND

**4.	XOR**

La compuerta XOR arroja como salida 1 cuando solo una entrada es 1, en otros casos arroja 0 como salida, entonces para obtenerla debemos combinar varias compuertas NAND.
Usamos primero una compuerta NAND que toma las entradas 'a' y 'b', y produce la salida 'nand1Out'. Luego la segunda compuerta NAND toma la entrada a y la salida de la primera compuerta NAND (nand1Out) como entradas, y produce una salida 'nand2Out'. Esto es básicamente la negación de la operación NAND entre 'a' y 'nand1Out', lo que es equivalente a la operación AND. Pasamos a la tercera compuerta NAND que toma la salida de la primera compuerta NAND (nand1Out) y la entrada 'b', y produce una salida 'nand3Out' produciendo una operación AND.Finalmente la última compuerta NAND toma las salidas 'nand2Out' y 'nand3Out' como entradas y produce la salida final out. De esta forma, se obtiene la compuerta XOR

**5.	MUX**

Para el chip MUX tenemos que la salida será 'a' si la señal del selector es '0' y 'b' en cualquier otro caso.
Entonces utilizamos una primera compuerta NAND con ambas entradas conectadas a la señal del selector (sel) y negamos su salida (nandSelOut). Luego se usa una compuerta NAND para realizar la operación lógica AND entre la entrada 'a' y la negación de sel (nandSelOut), produciendo la salida 'nand2Out'. Se utiliza otra compuerta NAND para realizar la operación AND entre la entrada 'b' y la señal de selección sel, produciendo la salida 'nand3Out'. Finalmente se utilizan dos compuertas NAND para realizar una operación OR entre las salidas 'nand2Out' y 'nand3Out' (que dependen de las entradas a y b, respectivamente), produciendo así la salida final del multiplexor (out).

**6.	DMUX**

El chip DMUX posee dos salidas las cuales serán igual a la entrada, 0 si la señal del selector es 0 y 0,entrada si la señal del selector es 1.
Empezamos con una compuerta NAND con ambas entradas conectadas a la señal de selección (sel). Como una NAND produce 0 cuando ambas entradas son 1, si sel es 1, nandSelOut (su negación) será 0, y si sel es 0, 'nandSelOut' será 1. Luego mediante una segunda compuerta NAND para realizar la operación lógica AND entre la entrada in y la negación de sel ('nandSelOut'), produciendo la salida 'nand2Out'. 
La salida 'nand2Out' se conecta de nuevo a la entrada a través de otra compuerta NAND, lo que invierte su valor. Se utiliza una cuarta compuerta NAND para realizar la operación AND entre la entrada in y la señal de selección sel, produciendo la salida 'nand4Out'. Esto significa que 'nand4Out' será igual a in solo cuando sel sea 1. Por ultimo La salida 'nand4Out' se conecta de nuevo a la entrada b a través de otra compuerta NAND, lo que invierte su valor y generando así el chip DMUX.

**7.	NOT16**

Este chip establece que, si un bit de entrada es 0, su salida correspondiente será 1, y si un bit de entrada es 1, su salida correspondiente será 0. Se diferenceia del chip Not únicamente en la cantidad de entradas y salidas que presenta (16), sin embargo, su funcionamiento básico es el mismo de la compuerta Not por lo que basta con implementar la compuerta Not previamente empleada para cada uno de los 16 bits del siguiente modo:

Not(in=in[i], out=out[i]);

En donde i va desde 0 hasta 15. De esta forma, cada bit se verá modificado de manera individual hasta obtener el resultado conjunto de los 16.

**8.	AND16**

El chip AND16 tiene que, si ambos bits de entrada son 1, el bit correspondiente en la salida out será 1; de lo contrario, será 0.

Se diferenceia del chip And únicamente en la cantidad de entradas y salidas que presenta (16), sin embargo, su funcionamiento básico es el mismo de la compuerta Not por lo que basta con implementar la compuerta And previamente empleada para cada uno de los 16 bits del siguiente modo:

And(in=in[i], in=in[i], out=out[i]);

En donde i va desde 0 hasta 15. De esta forma, cada bit se verá modificado de manera individual hasta obtener el resultado conjunto de los 16.

**9.	OR16**

Para el chip OR16 se tiene que, si al menos uno de los bits de entrada es 1, el bit correspondiente en la salida out será 1; de lo contrario, será 0.

Se diferenceia del chip Or únicamente en la cantidad de entradas y salidas que presenta (16), sin embargo, su funcionamiento básico es el mismo de la compuerta Not por lo que basta con implementar la compuerta Or previamente empleada para cada uno de los 16 bits del siguiente modo:

Or(in=in[i], in=in[i], out=out[i]);

En donde i va desde 0 hasta 15. De esta forma, cada bit se verá modificado de manera individual hasta obtener el resultado conjunto de los 16.

**10.	MUX16**

Este chip establece que si sel es 0, la salida out será igual a los bits de a; si sel es 1, la salida out será igual a los bits de b
Utilizando un MUX se realizan una operación de multiplexión entre un par de bits correspondientes de las entradas a y b. La señal de selección sel se utiliza para determinar cuál de las dos entradas (a o b) se envía a la salida out.
La salida de cada MUX de 1 bit  se asigna a un bit específico de la salida out, es decir, out[0] hasta out[15].
Para finalmente obtener como resultado una salida de 16 bits donde cada bit es seleccionado de forma independiente entre las entradas a y b basándose en el valor de la señal de selección sel.

**11.	OR8WAY**

El chip OR8WAY calcula la operación lógica OR para un conjunto de 8 bits de entrada, produciendo un solo bit de salida que es 1 si al menos uno de los bits de entrada es 1, y 0 si todos los bits de entrada son 0.primero,calculamos las operaciones OR para los pares de bits consecutivos (in[0] con in[1], in[2] con in[3], in[4] con in[5], in[6] con in[7]), lo que resulta en las salidas c, d, e, y f. Luego, se realizan más operaciones OR para combinar las salidas de las operaciones anteriores., c y d se combinan para producir g, y e y f se combinan para producir h y así para todos.
Finalmente, se realiza una última operación OR para combinar las salidas g y h, produciendo la salida final out.

**12.	MUX4WAY16**

El MUX4WAY16 realiza dos niveles de selección: primero selecciona entre las entradas a y b, y luego entre las entradas c y d, utilizando el bit de selección sel[0]. Luego, selecciona entre las salidas de estos dos primeros multiplexores (x y y) utilizando el bit de selección sel[1]. Esto permite seleccionar una de las cuatro entradas posibles (a, b, c, d) como salida, dependiendo del valor de los bits de selección sel[1] y sel[0].
Primero usamos un MUX16 para seleccionar entre las entradas a y b basándose en el bit de selección sel[0]. La salida de esta operación se asigna a la variable x.
Luego, usamos otro MUX16 para seleccionar entre las entradas c y d basándose también en el bit de selección sel[0]. La salida de esta operación se asigna a la variable y. Finalmente, se utiliza un tercer MUX16para seleccionar entre las salidas x y y basándose en el bit de selección sel[1]. La salida de este tercer multiplexor se asigna a la salida final out.

**13.	MUX8WAY16**

El chip MUX8WAY16 realiza tres niveles de selección: primero selecciona entre las cuatro entradas a, b, c y d, luego entre las cuatro entradas e, f, g y h, y finalmente entre las salidas de los dos primeros multiplexores (x e y), dependiendo del valor de los tres bits de selección (sel[2], sel[1], sel[0]). Esto permite seleccionar una de las ocho entradas posibles como salida.
Entonces en principio usamos un  Mux4Way16 para seleccionar entre las entradas a, b, c y d basándose en los dos bits menos significativos de la señal de selección (sel[1] y sel[0]). La salida de esta operación se asigna a la variable x.
Se repite este proceso con otro Mux4Way16 para seleccionar entre las entradas e, f, g y h basándose también en los dos bits menos significativos de la señal de selección (sel[1] y sel[0]). La salida de esta operación se asigna a la variable y.
Finalmente, se utiliza un ultimo Mux16 para seleccionar entre las salidas x e y basándose en el bit más significativo de la señal de selección (sel[2]). La salida de este tercer multiplexor se asigna a la salida final out.

**14.	DMUX4WAY**

el chip DMux4WAY toma una entrada de datos y la distribuye en cuatro salidas (a, b, c, d) basándose en dos señales de selección (sel[1] y sel[0]). Esto permite seleccionar una de las cuatro salidas posibles dependiendo de los valores de los bits de selección.
Primero utilizamos un DMux para distribuir la entrada in en dos salidas, x y y, basándose en el bit de selección sel[1].
Si sel[1] es 0, la entrada in se dirige a la salida x y la salida y se pone en 0.
Si sel[1] es 1, la entrada in se dirige a la salida y y la salida x se pone en 0.
Luego utilizamos dos DMux para distribuir las salidas x e y en cuatro salidas finales, a, b, c, y d, basándose en el bit de selección sel[0].
Si sel[0] es 0, la salida x se dirige a a y b, mientras que la salida y se dirige a c y d.
Si sel[0] es 1, la salida x se dirige a a y b, mientras que la salida y se dirige a c y d.

**15.	DMUX8WAY**

el chip DMux8WAY toma una entrada de datos y la distribuye en ocho salidas (a, b, c, d, e, f, g, h) basándose en tres señales de selección (sel[2], sel[1] y sel[0]). Esto permite seleccionar una de las ocho salidas posibles dependiendo de los valores de los bits de selección.
Entonces utilizamos DMux para distribuir la entrada in en dos salidas, x y y, basándose en el bit de selección sel[2].Si sel[2] es 0, la entrada in se dirige a la salida x y la salida y se pone en 0.Si sel[2] es 1, la entrada in se dirige a la salida y y la salida x se pone en 0.Luego, se utilizan DMux4Waypara distribuir las salidas x e y en ocho salidas finales (a, b, c, d, e, f, g, h), basándose en los bits de selección sel[1] y sel[0].
El primer DMux4Way distribuye las salidas x en cuatro salidas (a, b, c, d), basándose en los bits de selección sel[1] y sel[0].
El segundo DMux4Way distribuye las salidas y en las restantes cuatro salidas (e, f, g, h), también basándose en los bits de selección sel[1] y sel[0].
